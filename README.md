## How to run application
1. Clone repository
2. Go to application root folder
3. ```yarn install```
4. ```yarn start```
5. You can access application on ```localhost:3000```