import React, { useEffect, Fragment } from 'react';

import {
  USERS_PATH,
  LOGIN_PATH,
} from '../../shared/constants';

const CheckAuth = props => {
  const {
    location: {
      pathname,
    },
    location,
  } = window;

  const {
    children,
    blankPageOnNoAuth = true,
  } = props;

  const loginToken = localStorage.getItem('login_token');

  useEffect(() => {
    if (loginToken
      && pathname !== ''
      && pathname !== USERS_PATH
    ) {
      location.replace(USERS_PATH);
    } else if (!loginToken
      && pathname !== LOGIN_PATH
      && pathname !== `${LOGIN_PATH}/`
    ) {
      location.replace(LOGIN_PATH);
    }
  });

  return (
    <Fragment>
      {!loginToken && blankPageOnNoAuth
        ? null
        : children
      }
    </Fragment>
  );
};

export default CheckAuth;
