import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import CssBaseline from '@material-ui/core/CssBaseline';

import store from './store';

import Login from './pages/Login';
import Users from './pages/Users';

const App = () => (
  <div
    className="app"
  >
    <Provider
      store={store}
    >
      <CssBaseline />
      <BrowserRouter>
        <Switch>
          <Route
            exact
            path="/"
            component={Users}
          />
          <Route
            exact
            path="/login"
            component={Login}
          />
        </Switch>
      </BrowserRouter>
    </Provider>
  </div>
);

export default App;
