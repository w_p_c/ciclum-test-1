export const REQRES_DOMAIN = 'https://reqres.in';

export const USERS_PATH = '/';
export const LOGIN_PATH = '/login';
