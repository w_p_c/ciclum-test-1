import {
  SUCCESS_LOGIN, LOGOUT,
} from '../actions/types';

const initialState = null;

export default (state = initialState, action) => {
  const {
    type,
    payload,
  } = action;

  switch (type) {
    case SUCCESS_LOGIN: {
      const {
        token,
      } = payload;

      return token;
    }

    case LOGOUT:
      return initialState;

    default:
      return state;
  }
};
