import {
  put,
  call,
} from 'redux-saga/effects';

import {
  SUCCESS_LOGIN,
  FAILURE_LOGIN,
} from '../actions/types';

import {
  USERS_PATH,
} from '../../../shared/constants';

import login from '../services/login';

export default function* (action) {
  const {
    payload: {
      email,
      password,
    },
  } = action;

  try {
    const res = yield call(login, email, password);

    const {
      data: {
        token,
      },
    } = res;

    localStorage.setItem('login_token', token);

    yield put({
      type: SUCCESS_LOGIN,
      payload: {
        token,
      },
    });

    window.location.replace(USERS_PATH);
  } catch (error) {
    yield put({
      type: FAILURE_LOGIN,
      payload: error,
      error: true,
    });

    console.error(error);
  }
}
