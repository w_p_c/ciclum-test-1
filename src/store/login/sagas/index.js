import { takeEvery } from 'redux-saga/effects';

import {
  TRY_LOGIN,
} from '../actions/types';

import tryLogin from './tryLogin';

export default [
  takeEvery(TRY_LOGIN, tryLogin),
];
