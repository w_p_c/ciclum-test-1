import {
  TRY_LOGIN,
} from './types';

export default ({
  email,
  password,
}) => ({
  type: TRY_LOGIN,
  payload: {
    email,
    password,
  },
});
