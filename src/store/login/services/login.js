import axios from 'axios';

import {
  REQRES_DOMAIN,
} from '../../../shared/constants';

export default (email, password) => axios.post(
  `${REQRES_DOMAIN}/api/login`,
  {
    email,
    password,
  },
);
