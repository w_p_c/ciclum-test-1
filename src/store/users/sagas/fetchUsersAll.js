import {
  put,
  takeEvery,
} from 'redux-saga/effects';

import {
  SUCCESS_FETCH_USERS_ALL,
  FAILURE_FETCH_USERS_ALL,
  FETCH_USERS_PAGE,
  SUCCESS_FETCH_USERS_PAGE,
} from '../actions/types';

export default function* () {
  try {
    yield takeEvery(SUCCESS_FETCH_USERS_PAGE, function* (action) {
      const {
        payload: {
          page,
          total_pages: totalPages,
        },
      } = action;

      if (page < totalPages) {
        yield put({
          type: FETCH_USERS_PAGE,
          payload: {
            page: page + 1,
          },
        });
      }
    });

    yield put({
      type: FETCH_USERS_PAGE,
      payload: {
        page: 1,
      },
    });

    yield put({
      type: SUCCESS_FETCH_USERS_ALL,
    });
  } catch (error) {
    yield put({
      type: FAILURE_FETCH_USERS_ALL,
      payload: error,
      error: true,
    });
  }
}
