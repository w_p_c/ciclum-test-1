import {
  put,
  call,
} from 'redux-saga/effects';

import {
  SUCCESS_FETCH_USERS_PAGE,
  FAILURE_FETCH_USERS_PAGE,
} from '../actions/types';

import fetchUsersPage from '../services/fetchUsersPage';

export default function* (action) {
  try {
    const {
      payload: {
        page,
      },
    } = action;

    const req = yield call(fetchUsersPage, page);

    const {
      data: payload,
    } = req;

    yield put({
      type: SUCCESS_FETCH_USERS_PAGE,
      payload,
    });
  } catch (err) {
    yield put({
      type: FAILURE_FETCH_USERS_PAGE,
      payload: err,
      error: true,
    });
  }
}
