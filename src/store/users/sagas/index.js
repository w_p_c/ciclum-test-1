import {
  takeEvery,
} from 'redux-saga/effects';

import {
  FETCH_USERS_ALL,
  FETCH_USERS_PAGE,
} from '../actions/types';

import fetchUsersAll from './fetchUsersAll';
import fetchUsersPage from './fetchUsersPage';

export default [
  takeEvery(FETCH_USERS_ALL, fetchUsersAll),
  takeEvery(FETCH_USERS_PAGE, fetchUsersPage),
];
