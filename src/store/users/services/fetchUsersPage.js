import axios from 'axios';

import {
  REQRES_DOMAIN,
} from '../../../shared/constants';

export default page => axios.get(`${REQRES_DOMAIN}/api/users?page=${page}`);
