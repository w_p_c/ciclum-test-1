import {
  FETCH_USERS_ALL,
} from './types';

export default () => ({
  type: FETCH_USERS_ALL,
});
