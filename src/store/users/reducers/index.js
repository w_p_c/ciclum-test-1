import {
  SUCCESS_FETCH_USERS_PAGE,
} from '../actions/types';

const initialState = [];

export default (state = initialState, action) => {
  const {
    type,
    payload,
  } = action;

  switch (type) {
    case SUCCESS_FETCH_USERS_PAGE: {
      const {
        data,
      } = payload;

      return [...state, ...data];
    }

    default:
      return state;
  }
};
