import {
  all,
} from 'redux-saga/effects';

import login from './login/sagas';
import users from './users/sagas';

export default function* () {
  yield all([
    ...login,
    ...users,
  ]);
}
