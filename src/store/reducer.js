import { combineReducers } from 'redux';

import users from './users/reducers';
import login from './login/reducers';

export default combineReducers({
  users,
  login,
});
