import React, { useState } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import tryLogin from '../../store/login/actions/tryLogin';

import style from './style';

import CheckAuth from '../../components/CheckAuth';

const Login = props => {
  const {
    classes,
    tryLogin,
  } = props;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <CheckAuth
      blankPageOnNoAuth={false}
    >
      <div
        className={classNames(
          'login',
          classes.main,
        )}
      >
        <Paper
          className={classes.paper}
        >
          <Avatar
            className={classes.avatar}
          >
            <LockOutlinedIcon />
          </Avatar>
          <Typography
            component="h1"
            variant="h5"
          >
            Sign in
          </Typography>
          <form
            className={classes.form}
            onSubmit={e => {
              e.preventDefault();
              tryLogin({
                email,
                password,
              });
            }}
          >
            <FormControl
              margin="normal"
              required
              fullWidth
            >
              <InputLabel
                htmlFor="email"
              >
                Email Address
              </InputLabel>
              <Input
                id="email"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={e => setEmail(e.target.value)}
                value={email}
              />
            </FormControl>
            <FormControl
              margin="normal"
              required
              fullWidth
            >
              <InputLabel
                htmlFor="password"
              >
                Password
              </InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={e => setPassword(e.target.value)}
                value={password}
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign in
            </Button>
          </form>
        </Paper>
      </div>
    </CheckAuth>
  );
};

export default compose(
  withStyles(style),
  connect(
    null,
    {
      tryLogin,
    },
  ),
)(Login);
