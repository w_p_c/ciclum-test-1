import React, { useEffect } from 'react';
import classNames from 'classnames';
import { compose } from 'redux';
import { connect } from 'react-redux';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';

import {
  LOGIN_PATH,
} from '../../shared/constants';

import fetchUsersAll from '../../store/users/actions/fetchUsersAll';

import style from './style';

import CheckAuth from '../../components/CheckAuth';

const Users = props => {
  const {
    classes,
    fetchUsersAll,
    users,
  } = props;

  useEffect(() => {
    fetchUsersAll();
  }, []);

  return (
    <CheckAuth>
      <div
        className={classNames(
          'users',
          classes.main,
        )}
      >
        <AppBar
          position="static"
        >
          <Toolbar>
            <Typography
              variant="h6"
              color="inherit"
              className={classes.grow}
            >
              Users
            </Typography>
            <Button
              color="inherit"
              onClick={() => {
                localStorage.removeItem('login_token');
                window.location.replace(LOGIN_PATH);
              }}
            >
              Log Out
            </Button>
          </Toolbar>
        </AppBar>

        <List
          className={classNames(
            classes.list,
            classes.grow,
          )}
        >
          {users.map(
            ({
              id,
              avatar,
              first_name: firstName,
              last_name: lastName,
            }) =>
              ( // eslint-disable-line implicit-arrow-linebreak
                <ListItem
                  key={id}
                >
                  <ListItemAvatar>
                    <Avatar
                      src={avatar}
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary={`${firstName} ${lastName}`}
                  />
                </ListItem>
              ),
          )}
        </List>
      </div>
    </CheckAuth>
  );
};

export default compose(
  withStyles(style),
  connect(
    ({
      users,
    }) => ({
      users,
    }),
    {
      fetchUsersAll,
    },
  ),
)(Users);
