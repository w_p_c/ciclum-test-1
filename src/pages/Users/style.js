export default theme => {
  const {
    palette: {
      grey,
    },
  } = theme;

  return ({
    main: {
      width: '100vw',
      height: '100vh',
      display: 'flex',
      flexFlow: 'column nowrap',
      alignItems: 'center',
    },
    grow: {
      flexGrow: 1,
    },
    list: {
      background: grey[200],
      width: '50%',
      overflowY: 'auto',
    },
  });
};
